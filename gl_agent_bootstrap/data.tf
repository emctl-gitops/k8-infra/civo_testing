data "terraform_remote_state" "cluster_bootstrap" {
  backend = "http"

  config = {
    address  = var.cluster_bootstrap_remote_state_address
    username = var.cluster_bootstrap_username
    password = var.cluster_bootstrap_access_token
  }
}